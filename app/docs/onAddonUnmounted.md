onAddonUnmounted <small class="pull-right">*Shared*</small>
==============

The **onAddonUnmounted** callback is ran when an addon is unmounted by using [`addon.unmount()`](api/addon.unmount).
<br>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Parameters</h3>
  </div>

  | Name    | Type   | Description              |
  | ------- | ------ | ------------------------ |
  | `addon` | string | Addon that was unmounted |
</div>

onAddonMounted <small class="pull-right">*Shared*</small>
==============

The **onAddonMounted** callback is ran when an addon is mounted during engine
initialization or by using [`addon.mount()`](api/addon.mount).
<br>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Parameters</h3>
  </div>

  | Name    | Type   | Description            |
  | ------- | ------ | ---------------------- |
  | `addon` | string | Addon that was mounted |
</div>

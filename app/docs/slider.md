slider <small>_Inherits from [`scrollbar`](api/scrollbar)_</small>
======

Slider class

Constructor
-----------

* [`gui.slider()`](api/gui.slider)

Methods
-------

* [`slider:draw()`](api/slider.draw)
* [`slider:drawLabels()`](api/slider.drawLabels)
* [`slider:drawThumb()`](api/slider.drawThumb)
* [`slider:drawTrough()`](api/slider.drawTrough)
* [`slider:getMinLabel()`](api/slider.getMinLabel)
* [`slider:getMaxLabel()`](api/slider.getMaxLabel)
* [`slider:getThumbLength()`](api/slider.getThumbLength)
* [`slider:getThumbPos()`](api/slider.getThumbPos)
* [`slider:invalidateLayout()`](api/slider.invalidateLayout)
* [`slider:setMinLabel()`](api/slider.setMinLabel)
* [`slider:setMaxLabel()`](api/slider.setMaxLabel)
* [`slider:mousepressed()`](api/slider.mousepressed)
* [`slider:update()`](api/slider.update)
